//
//  PauseScene.h
//  PauseTest
//
//  Created by piepie on 11-4-30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "GameScene.h"
#import "MainScene.h"
@interface PauseScene : CCLayer 
{
	CCMenuItem *item_resumegame;
	CCMenuItem *item_retry;
	CCMenuItem *item_exit;
	CCSprite *p_pauseBoard;

}
+(id) scene;
- (void)initSound;
@end
