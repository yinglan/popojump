/*
 *  Constant.h
 *  FatJumper
 *
 *  Created by in-blue  on 10-11-9.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#define kMaxFlowTime		600
#define kMaxMap				4
#define kNumShape1			6
#define kNumShape2			10
#define kNumShape3			15
#define kNumShape4			7
#define kNumShape5			5
#define kNumShape6			6	
#define kNumShape7			6
#define kNumShape8			8
#define kNumShape9			8
#define kNumShape10			7
#define kNumShape11			10
#define kNumShape12			11

#define kSpeedBallon		0.2
#define kSpeedBad			0.2
#define kSpeedCoin			0.1
#define kSpeedNormal		0.1
#define kSpeedShoe			0.1
#define kSpeedWing			0.1
#define kSpeedBag           0.2
enum 
{
	
	
	kProp = 50,//道具图片
	kMan,     //人物
	kHightScoreLabel,//积分榜
	kNumLabel,//计数榜

	kBig,
	kHamburg,
	kBallon,
	kMoney,
	kRain,
	kLighting,
	kWing,
	kShoes,
	kDash,
	kBag,
	kBomb,
	kCoinLabel,
	kCoinLabel2,
	kBlock,
	kGhost,
	kFrozen,
};



