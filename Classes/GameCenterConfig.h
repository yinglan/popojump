//
//  GameCenterConfig.h
//  All Game
//
//  Created by o0402 on 11-5-19.
//  Copyright 2011 xiaoxin All rights reserved.
//

#import <UIKit/UIKit.h>		// UIKit.framework
#import <GameKit/GameKit.h> // GameKit.framework
#import "cocos2d.h"

@interface GameCenterConfig : NSObject<GKLeaderboardViewControllerDelegate,GKAchievementViewControllerDelegate> 
{
	UIViewController* m_pViewController;
}

// 单链实例
+ (id)shareGameCenterInstance;

/*********************************** GameCenter ************************************************/

// 初始化
- (void)initGameCenter;
- (void)initGameCenterWithViewController:(UIViewController*)viewController;

// 是否设备支持GameCenter
- (BOOL)isGameCenterAvailable;
// 登陆GameCenter
- (void)authenticateLocalPlayer;
// 设备AppleID被更换
- (void)gameCenterAuthenticationChanged; 

/*********************************** GameCenter ************************************************/


/*********************************** 排行榜 *****************************************************/

// 上传一个排行榜分
- (void)reportScore:(int64_t)score forCategory:(NSString*)category;

// 显示排行版
- (void)showLeaderboard;
- (void)showLeaderboardFromViewController:(UIViewController*)viewController;

// 显示成就
- (void)showAchievement;
- (void)showAchievementFromViewController:(UIViewController*)viewController;

// 下载排行榜前十分值
- (void)retrieveTopTenScores:(NSString*)category;

// 检索已登录用户好友列表
- (void)retrieveFriends;
// 获取好友的信息
- (void)loadPlayerData:(NSArray*)identifiers;

/*********************************** 排行榜 *****************************************************/


/*********************************** 成就 *******************************************************/

// 汇报一个成就的进度
- (void)reportAchievementIdentifier:(NSString*)identifier percentComplete:(float)percent;

// 得到所有的成就
- (void)loadAchievements;

// 根据ID获取成就
- (GKAchievement*)getAchievementForIdentifier:(NSString*)identifier;

// 获取成就描述和图片
- (NSArray*)retrieveAchievmentMetadata;

/*********************************** 成就 *******************************************************/

@end
