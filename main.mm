//
//  main.m
//  FatJumper
//
//  Created by in-blue  on 11-2-9.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"PopoJumpAppDelegate");
	[pool release];
	return retVal;
}
